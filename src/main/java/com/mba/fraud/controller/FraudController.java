package com.mba.fraud.controller;

import com.mba.fraud.messaging.FraudProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

    @RestController
    @RequestMapping(value = "/kafka")
    public class FraudController {
        private final FraudProducer fraudProducer;

        @Autowired
        public FraudController(FraudProducer fraudProducer) {
            this.fraudProducer = fraudProducer;
        }
        @PostMapping(value = "/publish")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.fraudProducer.sendMessage(message, "");
        }
    }