package com.mba.fraud.messaging;

import com.google.gson.Gson;
import com.mba.fraud.model.Fraud;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FraudListener {

    @Autowired
    FraudProducer fraudProducer;

    @Value("${topic-choreography}")
    private String choreographyTopic;

    @Value("${topic-out}")
    private String orchestratorTopic;

    private final Logger logger = LoggerFactory.getLogger(FraudListener.class);

    private String producerMessage;

    @KafkaListener(topics ="${topic-choreography}", groupId = "group-fraud")
    public void consumeChoreography(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Fraud fraud = convertStringToFraud(message);

        switch (fraud.getEventType()) {
            case "accounts.sucess":
                //producerMessage = "fraud.sucess";
                producerMessage = "fraud.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "accounts.failure":
                producerMessage = "fraud.undo.sucess";
                //producerMessage = "fraud.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "receipts.failure":
                producerMessage = "fraud.undo.sucess";
                //producerMessage = "fraud.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "transferSender.failure":
                producerMessage = "fraud.undo.sucess";
                //producerMessage = "fraud.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }
    }

    @KafkaListener(topics ="${topic-in}", groupId = "group-fraud")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Fraud fraud = convertStringToFraud(message);

        switch (fraud.getEventType()) {
            case "fraud.requested":
                //producerMessage = "fraud.sucess";
                producerMessage = "fraud.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
            case "undo.requested":
                producerMessage = "fraud.undo.sucess";
                //producerMessage = "fraud.undo.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
        }
    }

    private void callProducer(String message, String topic){

        fraudProducer.sendMessage(producerMessage, topic);
    }

    private Fraud convertStringToFraud(String message){
        Gson gson = new Gson();
        Fraud fraud = gson.fromJson(message, Fraud.class);

        return fraud;

    }
}
