package com.mba.fraud.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder

public class Fraud {

    public String eventType;
    public String timestamp;
}
